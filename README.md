# To-do app using jquery

In this to-do-app, user can
    - add to-do items
    - mark them as completed
    - delete them
    - view active and completed items
    - and order them based on priority

/* _Theming is under development_ */

