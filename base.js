$(document).ready(function () {
    todo();
    sortList();
});

function todo() {
    // Remove Item
    var itemsLeft = 0;
    $(document).on('click', '.todo-list-item .todo-cross-icon', function () {
        ele = $(this);
        if (!ele.closest('.todo-list-item').hasClass('completed')) {

            itemsLeft = ele.closest('.todo-container').find('.todo-list-wrapper ul li:not(.completed)').length - 1;
            ele.closest('.todo-container').find('.todo-action-wrapper .items-left em').text(itemsLeft);
        }
        if (!ele.closest('.todo-list-item').siblings().length > 0) {
            ele.closest('.todo-container').find('.todo-action-wrapper').removeClass('show');
        }
        ele.closest('.todo-list-item').remove();
    });
    //Select or unselect item
    $(document).on('click', '.todo-list-item .todo-status-icon', function () {
        ele = $(this);
        ele.closest('.todo-list-item').toggleClass('completed');
        itemsLeft = ele.closest('.todo-container').find('.todo-list-wrapper ul li:not(.completed)').length;
        ele.closest('.todo-container').find('.todo-action-wrapper .items-left em').text(itemsLeft);
    });
    //create an item
    $(document).on('keyup', '.todo-input-wrapper input', function (e) {
        ele = $(this);
        if (e.keyCode === 13) { //Enter
            let todoItemContent = ele.val();
            ele.closest('.todo-container').find('.todo-list-wrapper ul').append('<li class="todo-list-item ui-state-default"><i class="icon todo-status-icon"></i><span class="todo-item-content">' + todoItemContent + '</span><i class="icon todo-cross-icon"></i> </li>')
            ele.val('');
            ele.closest('.todo-container').find('.todo-action-wrapper').addClass('show');
            itemsLeft = ele.closest('.todo-container').find('.todo-list-wrapper ul li:not(.completed)').length;
            ele.closest('.todo-container').find('.todo-action-wrapper .items-left em').text(itemsLeft);
        }
    });

    $(document).on('click', '.todo-action-wrapper a', function () {
        ele = $(this);
        ele.siblings().removeClass('selected');
        ele.addClass('selected');
        if (ele.hasClass('all-item')) {
            ele.closest('.todo-container').find('.todo-list-item').removeClass('hide');
        }
        else if (ele.hasClass('active-item')) {
            ele.closest('.todo-container').find('.todo-list-item').removeClass('hide');
            ele.closest('.todo-container').find('.todo-list-item.completed').addClass('hide');
        }
        else if (ele.hasClass('completed-item')) {
            ele.closest('.todo-container').find('.todo-list-item').removeClass('hide');
            ele.closest('.todo-container').find('.todo-list-item:not(.completed)').addClass('hide');
        }
    });

    //Clear Completed
    $(document).on('click', '.clear-btn', function () {
        ele = $(this);
        ele.closest('.todo-container').find('.todo-list-item.completed').remove();
        if (!ele.closest('.todo-container').find('.todo-list-item').length > 0) {
            ele.closest('.todo-container').find('.todo-action-wrapper').removeClass('show');
        }
    });
}

//list drag and drop functionality
function sortList() {
    $("#sortable").sortable();
    $("#sortable").disableSelection();
}
